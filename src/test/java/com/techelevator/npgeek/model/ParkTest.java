package com.techelevator.npgeek.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParkTest {
	
Park park;
	
	@Before
	public void setup() {
		park = new Park();
	}
	
	@Test
	public void verify_park_code() {
		park.setParkCode("XV99");
		
		Assert.assertEquals("XV99", park.getParkCode());
	}
	
	@Test
	public void verify_blank_park_code() {
		Assert.assertEquals(null, park.getParkCode());
	}
	
	@Test
	public void verify_park_name() {
		park.setParkName("Turner Classic Park");
		
		Assert.assertEquals("Turner Classic Park", park.getParkName());
	}
	
	@Test
	public void verify_blank_park_name() {
		Assert.assertEquals(null, park.getParkCode());
	}
	
	@Test
	public void verify_state() {
		park.setState("Arkansas");
		
		Assert.assertEquals("Arkansas", park.getState());
	}
	
	@Test
	public void verify_blank_state() {
		Assert.assertEquals(null, park.getState());
	}
	
	@Test
	public void verify_acreage() {
		park.setAcreage(4401);
		
		Assert.assertEquals(4401, park.getAcreage());
	}
	
	@Test
	public void verify_blank_acreage() {
		Assert.assertEquals(0, park.getAcreage());
	}
	
	@Test
	public void verify_elevation_in_feet() {
		park.setElevationInFeet(45);
		
		Assert.assertEquals(45, park.getElevationInFeet());
	}
	
	@Test
	public void verify_blank_elevation_in_feet() {
		Assert.assertEquals(0, park.getElevationInFeet());
	}
	
	@Test
	public void verify_miles_of_trail() {
		park.setMilesOfTrail(4.55);
		
		Assert.assertEquals(4.55, park.getMilesOfTrail(), 0.0);
	}
	
	@Test
	public void verify_blank_miles_of_trail() {
		Assert.assertEquals(null, park.getMilesOfTrail());
	}
	
	@Test
	public void verify_number_of_campsites() {
		park.setNumberOfCampsites(5);
		
		Assert.assertEquals(5, park.getNumberOfCampsites());
	}
	
	@Test
	public void verify_blank_number_of_campsites() {
		Assert.assertEquals(0, park.getNumberOfCampsites());
	}
	
	@Test
	public void verify_climate() {
		park.setClimate("Hotter than hell");
		
		Assert.assertEquals("Hotter than hell", park.getClimate());
	}
	
	@Test
	public void verify_blank_climate() {
		Assert.assertEquals(null, park.getClimate());
	}
	
	@Test
	public void verify_year_founded() {
		park.setYearFounded(1945);
		
		Assert.assertEquals(1945, park.getYearFounded());
	}
	
	@Test
	public void verify_blank_year_founded() {
		Assert.assertEquals(0, park.getYearFounded());
	}
	
	@Test
	public void verify_annual_visitor_count() {
		park.setAnnualVisitorCount(445500);
		
		Assert.assertEquals(445500, park.getAnnualVisitorCount());
	}
	
	@Test
	public void verify_blank_annual_visitor_count() {
		Assert.assertEquals(0, park.getAnnualVisitorCount());
	}
	
	@Test
	public void verify_inspirational_quote() {
		park.setInspirationalQuote("Do or do not, there is no try.");
		
		Assert.assertEquals("Do or do not, there is no try.", park.getInspirationalQuote());
	}
	
	@Test
	public void verify_blank_inspirational_quote() {
		Assert.assertEquals(null, park.getInspirationalQuote());
	}
	
	@Test
	public void verify_inspirational_quote_source() {
		park.setInspirationalQuoteSource("Yoda");
		
		Assert.assertEquals("Yoda", park.getInspirationalQuoteSource());
	}
	
	@Test
	public void verify_blank_inspitrational_quote_source() {
		Assert.assertEquals(null, park.getInspirationalQuoteSource());
	}
	
	@Test
	public void verify_park_description() {
		park.setParkDescription("A large swath of dirt");
		
		Assert.assertEquals("A large swath of dirt", park.getParkDescription());
	}
	
	@Test
	public void verify_blank_park_description() {
		Assert.assertEquals(null, park.getParkDescription());
	}
	
	@Test
	public void verify_entry_fee() {
		park.setEntryFee(15);
		
		Assert.assertEquals(15, park.getEntryFee());
	}
	
	@Test
	public void verify_blank_entry_fee() {
		Assert.assertEquals(0, park.getEntryFee());
	}
	
	@Test
	public void verify_number_of_animal_species() {
		park.setNumberOfAnimalSpecies(500);
		
		Assert.assertEquals(500, park.getNumberOfAnimalSpecies());
	}
	
	@Test
	public void verify_blank_number_of_animal_species() {
		Assert.assertEquals(0, park.getNumberOfAnimalSpecies());
	}
}
