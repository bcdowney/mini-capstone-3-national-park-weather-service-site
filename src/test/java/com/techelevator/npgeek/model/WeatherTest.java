package com.techelevator.npgeek.model;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WeatherTest {
	
	Weather weather;
	
	@Before
	public void setup() {
		weather = new Weather();
	}
	
	@Test
	public void verify_park_code() {
		weather.setParkCode("TEST");
		
		Assert.assertEquals("TEST", weather.getParkCode());
	}
	
	@Test
	public void verify_five_day_forecast_value() {
		weather.setFiveDayForecastValue(99);
		
		Assert.assertEquals(99, weather.getFiveDayForecastValue());
	}
	
	@Test
	public void verify_low_in_fahrenheit() {
		weather.setLow(32);
		
		Assert.assertEquals(32, weather.getLow());
	}
	
	@Test
	public void verify_low_in_celcius() {
		weather.setCelsius(true);
		weather.setLow(32);
		
		Assert.assertEquals(0, weather.getLow());
	}
	
	@Test
	public void verify_high_in_fahrenheit() {
		weather.setHigh(32);
		
		Assert.assertEquals(32, weather.getHigh());
	}
	
	@Test
	public void verify_high_in_celcius() {
		weather.setCelsius(true);
		weather.setHigh(32);
		
		Assert.assertEquals(0, weather.getHigh());
	}
	
	@Test
	public void verify_forecast() {
		weather.setForecast("TEST");
		
		Assert.assertEquals("TEST", weather.getForecast());
	}
	
	@Test
	public void verify_start_celsius() {
		Assert.assertFalse(weather.isCelsius());
	}
	
	@Test
	public void verify_celcius() {
		weather.setCelsius(true);
		
		Assert.assertTrue(weather.isCelsius());
	}
	
	@Test
	public void verify_date() {
		weather.setFiveDayForecastValue(1);
		weather.setDay();
		
		Assert.assertEquals(LocalDate.now(), weather.getDay());
	}
	
	@Test
	public void verify_unadjusted_day() {
		weather.setDay();
		
		Assert.assertNotEquals(LocalDate.now(), weather.getDay());
	}
}
