package com.techelevator.npgeek.model;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

public class SurveyTest {
	
	Survey survey;
	
	@Before
	public void setup() {
		survey = new Survey();
	}
	
	@Test
	public void verify_survey_id() {
		survey.setSurveyId(99);
		
		Assert.assertEquals(99, survey.getSurveyId());
	}
	
	@Test
	public void verify_park_code() {
		survey.setParkCode("TEST");
		
		Assert.assertEquals("TEST", survey.getParkCode());
	}
	
	@Test
	public void verify_email_address() {
		survey.setEmailAddress("test@test.com");
		
		Assert.assertEquals("test@test.com", survey.getEmailAddress());
	}
	
	@Test
	public void verify_state() {
		survey.setState("TEST STATE");
		
		Assert.assertEquals("TEST STATE", survey.getState());
	}
	
	@Test
	public void verify_activity_level() {
		survey.setActivityLevel("TEST ACTIVITY");
		
		Assert.assertEquals("TEST ACTIVITY", survey.getActivityLevel());
	}
}
