package com.techelevator.npgeek.model.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.npgeek.model.Weather;

import junit.framework.Assert;

public class JDBCWeatherDAOIntegrationTest extends DAOIntegrationTest {

	private JDBCWeatherDAO weatherDAO;
	
	@Before
	public void setup() {
		weatherDAO = new JDBCWeatherDAO(super.getDataSource());
		insertTestPark();
		insertTestWeather();	
	}
	
	@Test
	public void ensure_weather_returns_by_park_code_in_fahrenheit() {
		List<Weather> weatherResults = weatherDAO.getWeatherByParkCode("TEST", false);
		int size = weatherResults.size();
		assertTrue("The method failed to return weather objects based on the query.", weatherResults.size() > 0);
		assertEquals("The high temperature is innacurate", 999, weatherResults.get(size - 1).getHigh());
		assertEquals("The low temperature is innacurate", 99, weatherResults.get(size - 1).getLow());
		assertFalse("The celsius boolean is innacurate", weatherResults.get(size - 1).isCelsius());
		assertNotEquals("The day is innaccurate", LocalDate.now(), weatherResults.get(size - 1).getDay());
	}
	
	@Test
	public void ensure_weather_returns_by_park_code_in_celcius() {
		List<Weather> weatherResults = weatherDAO.getWeatherByParkCode("TEST", true);
		int size = weatherResults.size();
		assertTrue("The method failed to return weather objects based on the query.", weatherResults.size() > 0);
		assertEquals("The high temperature is innacurate", 537, weatherResults.get(size - 1).getHigh());
		assertEquals("The low temperature is innacurate", 37, weatherResults.get(size - 1).getLow());
		assertTrue("The celsius boolean is innacurate", weatherResults.get(size - 1).isCelsius());
		assertNotEquals("The day is innaccurate", LocalDate.now(), weatherResults.get(size - 1).getDay());
	}
	
	@Test
	public void ensure_weather_does_not_return_with_invalid_park_code_in_fahrenheit() {
		List<Weather> weatherResults = weatherDAO.getWeatherByParkCode("TEST2", false);
		assertFalse("The getWeatherByParkCode method returned weather objects when it should have found no results.", weatherResults.size() > 0);
	}
	
	@Test
	public void ensure_weather_does_not_return_with_invalid_park_code_in_celcius() {
		List<Weather> weatherResults = weatherDAO.getWeatherByParkCode("TEST2", true);
		assertFalse("The getWeatherByParkCode method returned weather objects when it should have found no results.", weatherResults.size() > 0);
	}
	
	private void insertTestPark() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlParkInsert = "INSERT INTO park VALUES ('TEST', 'TEST PARK', 'FAKE STATE', 9999, 9999, 999.99, 99, 'TEST CLIMATE', 1900, 9999, 'TEST QUOTE', 'TEST QUOTE SOURCE', 'TEST DESCRIPTION', 99, 99)";
		jdbcTemplate.update(sqlParkInsert);
	}
	
	private void insertTestWeather() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlWeatherInsert = "INSERT INTO weather VALUES ('TEST', 99, 99, 999, 'GOOD')";
		jdbcTemplate.update(sqlWeatherInsert);
	}
	
}
