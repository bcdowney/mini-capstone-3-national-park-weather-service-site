package com.techelevator.npgeek.model.jdbc;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.npgeek.model.Park;

public class JDBCParkDAOIntegrationTest extends DAOIntegrationTest{
	
	private JDBCParkDAO parkDAO;
	private JdbcTemplate jdbcTemplate;
	
	@Before 
	public void setup() {
		parkDAO = new JDBCParkDAO(super.getDataSource());
		jdbcTemplate = new JdbcTemplate(super.getDataSource());
	}
	
	@Test
	public void get_all_parks() {
		String sql = "select count(*) from park ";
		SqlRowSet result = jdbcTemplate.queryForRowSet(sql);
		result.next();
		int count = result.getInt("count");
		Assert.assertTrue(count > 0);
		Assert.assertEquals(count, parkDAO.getAllParks().size());
	}
	
	@Test
	public void verify_content_of_park() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlParkInsert = "INSERT INTO park VALUES ('TEST', 'TEST PARK', 'FAKE STATE', 9999, 9999, 999.99, 99, 'TEST CLIMATE', 1900, 9999, 'TEST QUOTE', 'TEST QUOTE SOURCE', 'TEST DESCRIPTION', 99, 99)";
		jdbcTemplate.update(sqlParkInsert);
		List<Park> parks = parkDAO.getAllParks();
		int size = parks.size();
		
		Assert.assertTrue(size > 0);
		Assert.assertEquals("TEST", parks.get(size - 1).getParkCode());
		Assert.assertEquals("TEST PARK", parks.get(size - 1).getParkName());
		Assert.assertEquals("FAKE STATE", parks.get(size - 1).getState());
		Assert.assertEquals("TEST DESCRIPTION", parks.get(size - 1).getParkDescription());
		Assert.assertEquals(9999, parks.get(size - 1).getAcreage());
		Assert.assertEquals(9999, parks.get(size - 1).getElevationInFeet());
		Assert.assertEquals(999.99, parks.get(size - 1).getMilesOfTrail(), 0.0);
		Assert.assertEquals(99, parks.get(size - 1).getNumberOfCampsites());
		Assert.assertEquals("TEST CLIMATE", parks.get(size - 1).getClimate());
		Assert.assertEquals(1900, parks.get(size - 1).getYearFounded());
		Assert.assertEquals(9999, parks.get(size - 1).getAnnualVisitorCount());
		Assert.assertEquals("TEST QUOTE", parks.get(size - 1).getInspirationalQuote());
		Assert.assertEquals("TEST QUOTE SOURCE", parks.get(size - 1).getInspirationalQuoteSource());
		Assert.assertEquals("TEST DESCRIPTION", parks.get(size - 1).getParkDescription());
		Assert.assertEquals(99, parks.get(size - 1).getEntryFee());
		Assert.assertEquals(99, parks.get(size - 1).getNumberOfAnimalSpecies());
	}
	
	@Test
	public void verify_get_park_by_code() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlParkInsert = "INSERT INTO park VALUES ('TEST', 'TEST PARK', 'FAKE STATE', 9999, 9999, 999.99, 99, 'TEST CLIMATE', 1900, 9999, 'TEST QUOTE', 'TEST QUOTE SOURCE', 'TEST DESCRIPTION', 99, 99)";
		jdbcTemplate.update(sqlParkInsert);
		Park park = parkDAO.getParkByCode("TEST");
		
		Assert.assertFalse(park.toString().isEmpty());
		Assert.assertEquals("TEST", park.getParkCode());
		Assert.assertEquals("TEST PARK", park.getParkName());
		Assert.assertEquals("FAKE STATE", park.getState());
		Assert.assertEquals("TEST DESCRIPTION", park.getParkDescription());
		Assert.assertEquals(9999, park.getAcreage());
		Assert.assertEquals(9999, park.getElevationInFeet());
		Assert.assertEquals(999.99, park.getMilesOfTrail(), 0.0);
		Assert.assertEquals(99, park.getNumberOfCampsites());
		Assert.assertEquals("TEST CLIMATE", park.getClimate());
		Assert.assertEquals(1900, park.getYearFounded());
		Assert.assertEquals(9999, park.getAnnualVisitorCount());
		Assert.assertEquals("TEST QUOTE", park.getInspirationalQuote());
		Assert.assertEquals("TEST QUOTE SOURCE", park.getInspirationalQuoteSource());
		Assert.assertEquals("TEST DESCRIPTION", park.getParkDescription());
		Assert.assertEquals(99, park.getEntryFee());
		Assert.assertEquals(99, park.getNumberOfAnimalSpecies());
	}
	
	@Test
	public void verify_get_favorite_parks() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlParkInsert = "INSERT INTO park VALUES ('TEST', 'TEST PARK', 'FAKE STATE', 9999, 9999, 999.99, 99, 'TEST CLIMATE', 1900, 9999, 'TEST QUOTE', 'TEST QUOTE SOURCE', 'TEST DESCRIPTION', 99, 99);" +
					" delete from survey_result; " +
					" insert into survey_result values (default, 'TEST', 'j@rod.com', 'FAKE STATE', 'TEST ACTIVITY')";
		jdbcTemplate.update(sqlParkInsert);
		Map<Park, Integer> favPark = parkDAO.getFavoriteParks();
		List<Park> parks = new LinkedList<>();
		List<Integer> count = new LinkedList<>();
		for (Park p: favPark.keySet()) {
			parks.add(p);
			count.add(favPark.get(p));
		}
		int size = favPark.size();
		
		Assert.assertTrue(size > 0);
		Assert.assertEquals(1, count.get(0), 0.0);
		Assert.assertEquals("TEST", parks.get(0).getParkCode());
		Assert.assertEquals("TEST PARK", parks.get(0).getParkName());
		Assert.assertEquals("FAKE STATE", parks.get(0).getState());
	}
}
