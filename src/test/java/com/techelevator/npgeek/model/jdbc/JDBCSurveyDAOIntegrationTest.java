package com.techelevator.npgeek.model.jdbc;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.DAOIntegrationTest;
import com.techelevator.npgeek.model.Survey;
import com.techelevator.npgeek.model.Weather;

public class JDBCSurveyDAOIntegrationTest extends DAOIntegrationTest{
	
private JDBCSurveyDAO surveyDAO;
	
	@Before
	public void setup() {
		surveyDAO = new JDBCSurveyDAO(super.getDataSource());
		insertTestPark();
	}
	
	@Test
	public void ensure_save_survey_method_inserts_survey_info_into_table() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		surveyDAO.saveSurvey(createTestSurvey());
		String sqlSearchForTestSurvey = "SELECT * FROM survey_result WHERE parkcode = 'TEST'";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchForTestSurvey);
		assertTrue("The test query failed to return results for the test survey insertion", results.next());
		
		surveyDAO.saveSurvey(createTestSurvey());
		String sqlBadSearchForTestSurvey = "SELECT * FROM survey_result WHERE parkcode = 'TEST2'";
		SqlRowSet results2 = jdbcTemplate.queryForRowSet(sqlBadSearchForTestSurvey);
		assertFalse("The query returned results when there should have been none", results2.next());
	}
	
	private void insertTestPark() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(super.getDataSource());
		String sqlParkInsert = "INSERT INTO park VALUES ('TEST', 'TEST PARK', 'FAKE STATE', 9999, 9999, 999.99, 99, 'TEST CLIMATE', 1900, 9999, 'TEST QUOTE', 'TEST QUOTE SOURCE', 'TEST DESCRIPTION', 99, 99)";
		jdbcTemplate.update(sqlParkInsert);
	}

	private Survey createTestSurvey() {
		Survey newSurvey = new Survey();
		newSurvey.setSurveyId(9999);
		newSurvey.setParkCode("TEST");
		newSurvey.setEmailAddress("TEST@TEST.COM");
		newSurvey.setState("TESTSTATE");
		newSurvey.setActivityLevel("inactive");
		return newSurvey;
	}
}
