<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<c:import url="/WEB-INF/jsp/common/header.jsp" />

<section>
	<h3>Survey</h3>
	<div class="blurb">
	<c:url value ="/favoriteparks" var="favorites"/>
	<p>We're collecting information on what our patron's favorite parks are! Enter your information below and weigh in with your favorite park! Check the results <a href="${favorites}">HERE!</a></p>
	</div>
	<c:url value="/postsurvey" var="postSurvey"/>
	
	<form:form action="${postSurvey}" method="POST" modelAttribute="survey">
		<table class="surveyTable">
		<div>
			<tr>
			<td class="head"><label for="parkCode">Favorite National Park: </label></td>
			<td><form:select path="parkCode">
				<form:option value="" label="--Please Select--"/>
				<form:options items="${parks}" itemValue="parkCode" itemLabel="parkName"/>			
			</form:select>
		
				<form:errors path="parkCode" cssClass="error"/></td>
				</tr>
		</div>
		<div>
		<tr>
			<td class="head"><label for="emailAddress">Your email: </label></td>
			<td>	<form:input path="emailAddress"/>
					<form:errors path="emailAddress" cssClass="error"/></td>
			
			</tr>
		</div>
		<div>
		<tr>
			<td class="head"><label for="state">State of Residence: </label></td>
				<td><form:select path="state">
					<form:option value="" label="--Please Select--"/>
					<form:option value="AL" label="Alabama"/>
					<form:option value="AK" label="Alaska"/>
					<form:option value="AZ" label="Arizona"/>
					<form:option value="AR" label="Arkansas"/>
					<form:option value="CA" label="California"/>
					<form:option value="CO" label="Colorado"/>
					<form:option value="CT" label="Connecticut"/>
					<form:option value="DE" label="Delaware"/>
					<form:option value="DC" label="District Of Columbia"/>
					<form:option value="FL" label="Florida"/>
					<form:option value="GA" label="Georgia"/>
					<form:option value="HI" label="Hawaii"/>
					<form:option value="ID" label="Idaho"/>
					<form:option value="IL" label="Illinois"/>
					<form:option value="IN" label="Indiana"/>
					<form:option value="IA" label="Iowa"/>
					<form:option value="KS" label="Kansas"/>
					<form:option value="KY" label="Kentucky"/>
					<form:option value="LA" label="Louisiana"/>
					<form:option value="ME" label="Maine"/>
					<form:option value="MD" label="Maryland"/>
					<form:option value="MA" label="Massachusetts"/>
					<form:option value="MI" label="Michigan"/>
					<form:option value="MN" label="Minnesota"/>
					<form:option value="MS" label="Mississippi"/>
					<form:option value="MO" label="Missouri"/>
					<form:option value="MT" label="Montana"/>
					<form:option value="NE" label="Nebraska"/>
					<form:option value="NV" label="Nevada"/>
					<form:option value="NH" label="New Hampshire"/>
					<form:option value="NJ" label="New Jersey"/>
					<form:option value="NM" label="New Mexico"/>
					<form:option value="NY" label="New York"/>
					<form:option value="NC" label="North Carolina"/>
					<form:option value="ND" label="North Dakota"/>
					<form:option value="OH" label="Ohio"/>
					<form:option value="OK" label="Oklahoma"/>
					<form:option value="OR" label="Oregon"/>
					<form:option value="PA" label="Pennsylvania"/>
					<form:option value="RI" label="Rhode Island"/>
					<form:option value="SC" label="South Carolina"/>
					<form:option value="SD" label="South Dakota"/>
					<form:option value="TN" label="Tennessee"/>
					<form:option value="TX" label="Texas"/>
					<form:option value="UT" label="Utah"/>
					<form:option value="VT" label="Vermont"/>
					<form:option value="VA" label="Virginia"/>
					<form:option value="WA" label="Washington"/>
					<form:option value="WV" label="West Virginia"/>
					<form:option value="WI" label="Wisconsin"/>
					<form:option value="WY" label="Wyoming"/>
					<form:option value="AS" label="American Samoa"/>
					<form:option value="GU" label="Guam"/>
					<form:option value="MP" label="Northern Mariana Islands"/>
					<form:option value="PR" label="Puerto Rico"/>
					<form:option value="UM" label="United States Minor Outlying Islands"/>
					<form:option value="VI" label="Virgin Islands"/>
				</form:select>
					<form:errors path="state" cssClass="error"/></td>
					</tr>
		</div>
		<div>
		<tr>
			<td class="head"><label for="activityLevel">Activity Level: </label></td>
				<td><form:radiobutton path="activityLevel" value="Inactive"/>Inactive
				<form:radiobutton path="activityLevel" value="Sedentary"/>Sedentary
				<form:radiobutton path="activityLevel" value="Active"/>Active
				<form:radiobutton path="activityLevel" value="Extremely Active"/>Extremely Active
					<form:errors path="activityLevel" cssClass="error"/></td>
					</tr>
		</div>
		</table>
		<div class="button"><input type="submit" value="Submit"/> </div>
	</form:form>
</section>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />