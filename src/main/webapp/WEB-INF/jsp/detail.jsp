<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<c:import url="/WEB-INF/jsp/common/header.jsp" />

<section>
	<c:url value="/img/parks/${fn:toLowerCase(detailPark.parkCode)}.jpg" var="parkImage"/>
	<figure>
		<img src="${parkImage}">
		<figcaption>
			<a class ="quote"><c:out  value="${detailPark.inspirationalQuote}"/></a>
			<br>
			<a class="quoteSource" style="text-align: right"><c:out value="-${detailPark.inspirationalQuoteSource}"/></a>
		</figcaption>
	</figure>
	<div>
		<h4><c:out value="${detailPark.parkName} (${detailPark.parkCode})"/></h4>
		<h5><c:out value="${detailPark.state}"/></h5>
	</div>
	<div>
		<p><c:out value="${detailPark.parkDescription}"/></p>
	</div>
	<div style="display:flex;justify-content:space-between">
		<ul>
			<li>Founded: <c:out value="${detailPark.yearFounded}"/></li>
			<li>Acreage: <c:out value="${detailPark.acreage}"/> acres</li>
			<li>Elevation: <c:out value="${detailPark.elevationInFeet}"/> ft.</li>
		</ul>
		<ul>
			<li>Trails: <c:out value="${detailPark.milesOfTrail}"/> mi.</li>
			<li>Climate: <c:out value="${detailPark.climate}"/></li>
			<li>Campsites: <c:out value="${detailPark.numberOfCampsites}"/></li>
			
		</ul>
		<ul>
			<fmt:formatNumber type="number" value="${detailPark.annualVisitorCount}" var="annualVisitors"/>
			<li>Annual Visitors: <c:out value="${annualVisitors}"/></li>
			<li>Animal Species: <c:out value="${detailPark.numberOfAnimalSpecies}"/></li>
			<li>Entry Fee: <b><c:out value="$${detailPark.entryFee}.00"/></b></li>
		</ul>
	</div>
	<hr style="width:93%">
	<div class="weather">
		<div class="forecastHead">
			<div class = "headContent"><h2>Five Day Forecast</h2>
			<div class="tempSwitch">F<label class="switch">
	  			<c:if test="${!celsius}">
	  				<input type="checkbox"/>
	  			</c:if>
	  			<c:if test="${celsius}">
	  				<input type="checkbox" checked="checked"/>
	  			</c:if>
	  			<span class="slider round"></span>
			</label>C &nbsp
			<c:url value="/switchunits" var="switchUnits"/>
			<form action="${switchUnits}" method="GET">
				<input type="hidden" name="parkCode" value="${detailPark.parkCode}"/>
				<input type="submit" value="Apply"/>
			</form>
			</div>
			</div>
		</div>
		<div class="daysCast">
			<c:forEach items="${weather}" var="forecast">
			<fmt:parseDate value="${forecast.day}" var= "day" pattern="yyyy-MM-dd"/>
			<fmt:formatDate value="${day}" var= "day" pattern="MM/dd"/>
			<c:url value="/img/weather/${fn: replace (forecast.forecast, 'partly cloudy', 'partlyCloudy')}.png" var="forecastImage"/>
				<div class="today">
					<c:if test="${forecast.fiveDayForecastValue == 1}">
					
						<h4>Today</h4>
						<h4><c:out value="${day}"/></h4>
						<img src="${forecastImage}">
						<div class="todayTemp">
							<label><b>High</b> <c:out value="${forecast.high}�"/></label>&nbsp
							&nbsp<label><b>Low </b><c:out value="${forecast.low}�"/></label>
						</div>
						<c:if test="${forecast.forecast == 'snow'}">
							<p>Make sure to pack your snowshoes!</p>
						</c:if>
						<c:if test="${forecast.forecast == 'rain'}">
							<p>Pack your rain gear and a good pair of waterproof shoes!</p>
						</c:if>
						<c:if test="${forecast.forecast == 'thunderstorms'}">
							<p>Seek shelter and avoid hiking on exposed ridges.</p>
						</c:if>
						<c:if test="${forecast.forecast == 'sunny'}">
							<p>Don't forget your sunblock!</p>
						</c:if>
						<c:if test="${!celsius}">
							<c:if test="${forecast.high > 75}">
								<p>With the high temperatures, be sure to pack an extra gallon of water.</p>
							</c:if>
							<c:if test="${(forecast.high - forecast.low) > 20}">
								<p>Be sure to wear breathable layers as the temperature is going to vary by more than 20�.</p>
							</c:if>
							<c:if test="${forecast.low < 20}">
								<p>Take precautions against today's excessively low temperatures. Protect your extremities and be aware of the symptoms of frostbite and hypothermia. </p>
							</c:if>
						</c:if>
						<c:if test="${celsius}">
							<c:if test="${forecast.high > 24}">
								<p>With the high temperatures, be sure to pack an extra gallon of water.</p>
							</c:if>
							<c:if test="${(forecast.high - forecast.low) > 10}">
								<p>Be sure to wear breathable layers as the temperature is going to vary by more than 10�.</p>
							</c:if>
							<c:if test="${forecast.low < -7}">
								<p>Take precautions against today's excessively low temperatures. Protect your extremities and be aware of the symptoms of frostbite and hypothermia. </p>
							</c:if>
						</c:if>
					</c:if>
				</div>
				<div class="notToday">
					<c:if test="${forecast.fiveDayForecastValue != 1}">
						<c:out value="${day}"/>
						<img src="${forecastImage}">
						<div class="notTodayTemp">
							<label><b>High</b> <c:out value="${forecast.high}�"/></label>
							<label><b>Low</b> <c:out value="${forecast.low}�"/></label>
						</div>
					</c:if>
				</div>
			</c:forEach>
		</div>
		
	</div>
	
</section>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />