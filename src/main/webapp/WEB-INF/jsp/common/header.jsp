<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="UTF-8">
		<title>National Park Geek</title>
		<c:url value="/css/site.css" var="cssHref" />
    	<link rel="stylesheet" href="${cssHref}">
    	<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
	</head>
	<body>
		<header>
    		<c:url value="/" var="homePageHref" />
    		<c:url value="/img/logo.png" var="logoSrc" />
    		<c:url value="/survey" var="survey"/>
        <a href="${homePageHref}">
        		<img src="${logoSrc}" alt="National Park Geek logo" />
        </a>
        </header>
        <nav>
	        <ul>
	        	<li><a href="${homePageHref}">Home</a></li>
	        	<li><a href="${survey}">Survey</a></li>
	        </ul>
        </nav>

