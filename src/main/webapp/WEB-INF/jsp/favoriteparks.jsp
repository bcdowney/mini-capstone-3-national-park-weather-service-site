<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<c:import url="/WEB-INF/jsp/common/header.jsp" />

<section>
	<h3>Favorite National Parks</h3>
	<p>These are the favorite National Parks as chosen by our visitors. Is yours one of them?</p>
	<div>
		<c:forEach items="${favoriteParks}" var="favoritePark">
			<c:url value="/detail" var="detailPage">
				<c:param name="parkCode" value="${favoritePark.key.parkCode}"></c:param>
			</c:url>
			<c:url value="/img/parks/${fn:toLowerCase(favoritePark.key.parkCode)}.jpg" var="parkImage"/>
			
			<c:if test="${favoritePark.value > 0}">
				
				<div class="favoritePark">
				<tr>
					<td><a href="${detailPage}"><img src="${parkImage}"></a></td>
					<td><a href="${detailPage}"><h3><c:out value="${favoritePark.key.parkName}"/></h3></a></td>
					<td><h3><c:out value="Favorited ${favoritePark.value} time(s)!"/></h3></td>
					</tr>
				</div>
			</c:if>
		</c:forEach>
	</div>
</section>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />