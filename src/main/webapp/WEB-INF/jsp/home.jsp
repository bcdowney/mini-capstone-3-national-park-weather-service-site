<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<c:import url="/WEB-INF/jsp/common/header.jsp" />

<section>
	
	<c:forEach items="${parks}" var="park">
		<c:url value="/detail" var="detailPage">
			<c:param name="parkCode" value="${park.parkCode}"></c:param>
		</c:url>
		<div class="homePark">
			<c:url value="/img/parks/${fn:toLowerCase(park.parkCode)}.jpg" var="parkImage"/>
			<a class="homeImage" href="${detailPage}"><img src="${parkImage}"></a>
			<div class="homeText">
				<a href="${detailPage}"><h4><c:out value="${park.parkName}"/></h4></a>
				<h5><c:out value="${park.state}"></c:out></h5>
				<p><c:out value="${park.parkDescription}"></c:out></p>
			</div>
		</div>
	</c:forEach>

</section>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />