package com.techelevator.npgeek.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.npgeek.model.ParkDAO;
import com.techelevator.npgeek.model.Survey;
import com.techelevator.npgeek.model.SurveyDAO;
import com.techelevator.npgeek.model.WeatherDAO;

@SessionAttributes("celsius")
@Controller
public class NPGeekController {
	
	@Autowired
	private ParkDAO parkDao;
	@Autowired
	private WeatherDAO weatherDao;
	@Autowired
	private SurveyDAO surveyDao;
	
	@RequestMapping("/")
	public String displayHomePage(HttpServletRequest request) {
		request.setAttribute("parks", parkDao.getAllParks());
		return "home";
	}
	
	@RequestMapping("/detail")
	public String displayDetailPage(HttpServletRequest request, @RequestParam String parkCode, ModelMap map) {
		if (!map.containsAttribute("celsius")) {
			map.addAttribute("celsius", false);
		}
		if ((boolean) map.get("celsius")) {
			request.setAttribute("weather", weatherDao.getWeatherByParkCode(parkCode, true));
		} else {
			request.setAttribute("weather", weatherDao.getWeatherByParkCode(parkCode, false));
		}
		request.setAttribute("detailPark", parkDao.getParkByCode(parkCode));
		return "detail";
	}
	
	@RequestMapping("/switchunits")
	public String switchTemperatureUnits(@RequestParam String parkCode, ModelMap map) {
		boolean celsius = (boolean) map.get("celsius");
		map.addAttribute("celsius", !celsius);
		map.addAttribute("parkCode", parkCode);
		return "redirect:/detail";
	}

	@RequestMapping("/survey")
	public String displaySurveyPage(Model model) {
		if (!model.containsAttribute("survey")) {
			model.addAttribute("survey", new Survey());
		}
		model.addAttribute("parks", parkDao.getAllParks());
		return "survey";
	}
	
	@RequestMapping("/favoriteparks")
	public String displayFavoriteParksPage(HttpServletRequest request) {
		request.setAttribute("favoriteParks", parkDao.getFavoriteParks());
		return "favoriteparks";
	}
	
	@RequestMapping(path="/postsurvey", method=RequestMethod.POST)
	public String postUserSurvey(@Valid @ModelAttribute("survey") Survey survey, BindingResult result, Model model) {
		if (result.hasErrors()) {
			if (!model.containsAttribute("survey")) {
				model.addAttribute("survey", new Survey());
			}
			model.addAttribute("parks", parkDao.getAllParks());
			return "survey";
		}
		surveyDao.saveSurvey(survey);
		return "redirect:/favoriteparks";
	}
	
}
