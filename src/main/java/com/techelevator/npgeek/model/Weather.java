package com.techelevator.npgeek.model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Weather {

	private String parkCode;
	private int fiveDayForecastValue;
	private int low;
	private int high;
	private String forecast;
	private boolean celsius;
	private LocalDate day;
	
	public String getParkCode() {
		return parkCode;
	}
	
	public void setParkCode(String parkCode) {
		this.parkCode = parkCode;
	}
	
	public int getFiveDayForecastValue() {
		return fiveDayForecastValue;
	}
	
	public void setFiveDayForecastValue(int fiveDayForecastValue) {
		this.fiveDayForecastValue = fiveDayForecastValue;
	}
	
	public int getLow() {
		return low;
	}
	
	public void setLow(int low) {
		if (celsius) {
			this.low = convertToCelsius(low);	
		} else {
		this.low = low;	
		}
	}
	
	public int getHigh() {
		return high;
	}
	
	public void setHigh(int high) {
		if (celsius) {
			this.high = convertToCelsius(high);
		} else {
			this.high = high;
		}
	}
	
	public String getForecast() {
		return forecast;
	}
	
	public void setForecast(String forecast) {
		this.forecast = forecast;
	}
	
	public boolean isCelsius() {
		return celsius;
	}
	
	public LocalDate getDay() {
		return day;
	}

	public void setDay() {
		this.day = convertDay(fiveDayForecastValue);
	}

	public void setCelsius(boolean celsius) {
		this.celsius = celsius;
		setHigh(this.high);
		setLow(this.low);
	}
	
	private int convertToCelsius(int temp) {
		int result = (int) ((temp - 32) / 1.8);
		return result;
	}
	
	private LocalDate convertDay(int fiveDayForecastValue) {
		long amount = (fiveDayForecastValue - 1);
		
		LocalDate date= ChronoUnit.DAYS.addTo(LocalDate.now(), amount);
		return date;
	}
}
