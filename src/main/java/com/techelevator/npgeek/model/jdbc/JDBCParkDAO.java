package com.techelevator.npgeek.model.jdbc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.model.Park;
import com.techelevator.npgeek.model.ParkDAO;

@Component
public class JDBCParkDAO implements ParkDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Park> getAllParks() {
		String selectParks = "SELECT * FROM park";
		SqlRowSet results = jdbcTemplate.queryForRowSet(selectParks);
		return mapRowToPark(results);
	}

	@Override
	public Park getParkByCode(String parkCode) {
		Park park = new Park();
		String selectPark = "select * from park "+
							"where parkcode = ?;";
		SqlRowSet results = jdbcTemplate.queryForRowSet(selectPark, parkCode);
		List<Park> parks = mapRowToPark(results);
		park = parks.get(0);
		return park;
	}

	@Override
	public Map<Park, Integer> getFavoriteParks() {
		String sqlFavorite = "select p.parkcode, parkname, p.state, count(s.parkcode) from park p " +
				"left join survey_result s on p.parkcode = s.parkcode " +
				"group by p.parkcode " +
				"order by count desc, parkname; ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFavorite);
		return mapRowToFavoritePark(results);
	}
	
	private LinkedList<Park> mapRowToPark(SqlRowSet result){
		LinkedList<Park> parks = new LinkedList<>();
		while(result.next()) {
			Park park = new Park();
			park.setParkCode(result.getString("parkcode"));
			park.setParkName(result.getString("parkname"));
			park.setState(result.getString("state"));
			park.setAcreage(result.getInt("acreage"));
			park.setElevationInFeet(result.getInt("elevationinfeet"));
			park.setMilesOfTrail(result.getDouble("milesoftrail"));
			park.setNumberOfCampsites(result.getInt("numberofcampsites"));
			park.setClimate(result.getString("climate"));
			park.setYearFounded(result.getInt("yearfounded"));
			park.setAnnualVisitorCount(result.getInt("annualvisitorcount"));
			park.setInspirationalQuote(result.getString("inspirationalquote"));
			park.setInspirationalQuoteSource(result.getString("inspirationalquotesource"));
			park.setParkDescription(result.getString("parkdescription"));
			park.setEntryFee(result.getInt("entryfee"));
			park.setNumberOfAnimalSpecies(result.getInt("numberofanimalspecies"));
			parks.add(park);
		}
		return parks;
	}
	
	private Map<Park, Integer> mapRowToFavoritePark(SqlRowSet result){
		Map<Park, Integer> favParks = new LinkedHashMap<Park, Integer>();
		while(result.next()) {
			Park park = new Park();
			int count = result.getInt("count");
			park.setParkCode(result.getString("parkcode"));
			park.setParkName(result.getString("parkname"));
			park.setState(result.getString("state"));
			favParks.put(park, count);
		}
		return favParks;
	}

}
