package com.techelevator.npgeek.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.model.Weather;
import com.techelevator.npgeek.model.WeatherDAO;

@Component
public class JDBCWeatherDAO implements WeatherDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCWeatherDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Weather> getWeatherByParkCode(String parkCode, boolean celsius) {
		List<Weather> weatherByPark = new ArrayList<Weather>();
		String sqlWeatherQuery = "SELECT * FROM weather " + 
								"WHERE parkcode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlWeatherQuery, parkCode);
		while(results.next()) {
			weatherByPark.add(mapRowToWeather(results, celsius));
		}
		return weatherByPark;
	}
	
	private Weather mapRowToWeather(SqlRowSet results, boolean celsius) {
		Weather newWeather = new Weather();
		newWeather.setParkCode(results.getString("parkcode"));
		newWeather.setFiveDayForecastValue(results.getInt("fivedayforecastvalue"));
		newWeather.setLow(results.getInt("low"));
		newWeather.setHigh(results.getInt("high"));
		newWeather.setForecast(results.getString("forecast"));
		newWeather.setCelsius(celsius);
		newWeather.setDay();
		return newWeather;
	}

}
