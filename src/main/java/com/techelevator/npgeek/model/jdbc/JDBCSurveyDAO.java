package com.techelevator.npgeek.model.jdbc;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.techelevator.npgeek.model.Survey;
import com.techelevator.npgeek.model.SurveyDAO;

@Component
public class JDBCSurveyDAO implements SurveyDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCSurveyDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveSurvey(Survey newSurvey) {
		String sqlSurveyInsert = "INSERT INTO survey_result VALUES (default, ?, ?, ?, ?)";
		jdbcTemplate.update(sqlSurveyInsert, newSurvey.getParkCode(), 
				newSurvey.getEmailAddress(), newSurvey.getState(), 
				newSurvey.getActivityLevel());
	}

}
