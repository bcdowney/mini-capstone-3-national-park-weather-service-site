# Mini Capstone 3 - National Park Weather Service Site

A mock National Parks website utilizing Spring Web MVC combined with JDBC, PostgreSQL, and a Tomcat server to render information per mock client specifications.

*Please see the two pdfs in the ETC folder for the specific requirements this was built to meet.